"""Produce roc curves from tagger output and labels."""
import numpy as np
import h5py

from puma import Roc, RocPlot
from puma.metrics import calc_rej
from puma.utils import get_good_colours, get_good_linestyles, logger

files_sig_deltar = ["/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697652._000001.output.h5",
                   "/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697652._000002.output.h5",
                   "/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697652._000003.output.h5",
                   "/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697652._000005.output.h5"]

                    
files_bkg_deltar = ["/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697649._000001.output.h5",
"/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697650._000001.output.h5",
"/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697650._000002.output.h5",
"/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697650._000003.output.h5",
"/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697650._000005.output.h5",
"/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697651._000001.output.h5",
"/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697651._000002.output.h5",
"/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697651._000003.output.h5",
"/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697651._000005.output.h5",
"/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697649._000002.output.h5",
"/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697649._000003.output.h5",
"/project/gruppo1/atlas_gen/rambeluc/atlasplot/deltaR_1.0/user.lrambell.33697649._000005.output.h5"]

files_sig_ghost = ['/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730407._000001.output.h5',
                  '/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730407._000002.output.h5',
                  '/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730407._000003.output.h5',
                  '/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730407._000005.output.h5']

files_bkg_ghost = ['/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730409._000001.output.h5',
                   '/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730408._000001.output.h5',
                   '/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730410._000001.output.h5',
                   '/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730409._000002.output.h5',
                   '/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730409._000003.output.h5',
                   '/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730409._000005.output.h5',
                   '/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730408._000002.output.h5',
                   '/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730408._000003.output.h5',
                   '/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730408._000005.output.h5',
                   '/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730410._000002.output.h5',
                   '/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730410._000003.output.h5',
                   '/project/gruppo1/atlas_gen/rambeluc/atlasplot/ghostAss_test/user.lrambell.33730410._000005.output.h5'
                   ]

PHBB_HBB_deltar = []
PHBB_QCD_deltar = []
NJETS_deltar = []

DISC_HBB_deltar = []
DISC_QCD_deltar = []

for file in files_sig_deltar:
    with h5py.File(file, 'r') as f:
        #print(f['jets'].dtype)
        is_Hbb = f['jets']['R10TruthLabel_R22v1'] == 11
        is_QCD = f['jets']['R10TruthLabel_R22v1'] == 10
        pHbb = f['jets']['GN2Xv00_phbb']
        pQcd = f['jets']['GN2Xv00_pqcd']

        pHbb_hbb = pHbb[is_Hbb].tolist()
        pHbb_qcd = pHbb[is_QCD].tolist()
        pQcd_hbb = pQcd[is_Hbb].tolist()
        pQcd_qcd = pQcd[is_QCD].tolist()
        
        A = np.log(pHbb/pQcd)
        A_sig = A[is_Hbb].tolist()
        A_bkg = A[is_QCD].tolist()

        n_jets= pHbb.shape[0]
        NJETS_deltar.append(n_jets)
        DISC_HBB_deltar = np.append(DISC_HBB_deltar, A_sig)
        DISC_QCD_deltar = np.append(DISC_QCD_deltar, A_bkg)



for file in files_bkg_deltar:
        
        with h5py.File(file, 'r') as f:
            is_Hbb = f['jets']['R10TruthLabel_R22v1'] == 11
            is_QCD = f['jets']['R10TruthLabel_R22v1'] == 10
            pHbb = f['jets']['GN2Xv00_phbb']
            pQcd = f['jets']['GN2Xv00_pqcd']

            A = np.log(pHbb/pQcd)
            A_sig = A[is_Hbb].tolist()
            A_bkg = A[is_QCD].tolist()
            
            n_jets= pHbb.shape[0]
            
            NJETS_deltar.append(n_jets)
            DISC_HBB_deltar = np.append(DISC_HBB_deltar, A_sig)
            DISC_QCD_deltar = np.append(DISC_QCD_deltar, A_bkg)



    
sig_eff = np.linspace(0.4, 1, 100)

Hbb_rej_deltar = calc_rej(np.asarray(DISC_HBB_deltar), np.asarray(DISC_QCD_deltar), sig_eff, return_cuts=True)

PHBB_HBB_ghost = []
PHBB_QCD_ghost = []
NJETS_ghost = []

DISC_HBB_ghost = []
DISC_QCD_ghost = []
for file in files_sig_ghost:
     
    with h5py.File(file, 'r') as f:
        #print(f['jets'].dtype)
        is_Hbb = f['jets']['R10TruthLabel_R22v1'] == 11
        is_QCD = f['jets']['R10TruthLabel_R22v1'] == 10
        pHbb = f['jets']['GN2Xv00_phbb']
        pQcd = f['jets']['GN2Xv00_pqcd']

        A = np.log(pHbb/pQcd)
        A_sig = A[is_Hbb].tolist()
        A_bkg = A[is_QCD].tolist()

        n_jets= pHbb.shape[0]
       
        NJETS_ghost.append(n_jets)
        DISC_HBB_ghost = np.append(DISC_HBB_ghost, A_sig)
        DISC_QCD_ghost = np.append(DISC_QCD_ghost, A_bkg)


for file in files_bkg_ghost:
        
        with h5py.File(file, 'r') as f:
            is_Hbb = f['jets']['R10TruthLabel_R22v1'] == 11
            is_QCD = f['jets']['R10TruthLabel_R22v1'] == 10
            pHbb = f['jets']['GN2Xv00_phbb']
            pQcd = f['jets']['GN2Xv00_pqcd']

            A = np.log(pHbb/pQcd)
            A_sig = A[is_Hbb].tolist()
            A_bkg = A[is_QCD].tolist()

            n_jets= pHbb.shape[0]
            NJETS_ghost.append(n_jets)
            DISC_HBB_ghost = np.append(DISC_HBB_ghost, A_sig)
            DISC_QCD_ghost = np.append(DISC_QCD_ghost, A_bkg)

Hbb_rej_ghost = calc_rej(np.asarray(DISC_HBB_ghost), np.asarray(DISC_QCD_ghost), sig_eff, return_cuts=True)

plot_roc = {
    key : RocPlot(
        n_ratio_panels=1,
        ylabel="Background rejection",
        xlabel=f"{key}-jet efficiency",
        atlas_first_tag="Simulation Work in progress",
        atlas_second_tag="$\\sqrt{s}=13$ TeV, Xbb jets",
        figsize=(6.5, 6),
        y_scale=1.4,
        title = "QCD rejection"
    ) for key in ['Hbb']}

print(np.sum(NJETS_ghost), np.sum(NJETS_deltar), 'JET NUMBERRR')
linestyles = get_good_linestyles()[:6]
for key, value in plot_roc.items():

    value.add_roc(
        Roc(
            sig_eff,
            Hbb_rej_deltar[0],
            n_test= np.sum(NJETS_deltar),
            rej_class="qcd",
            signal_class=f"{key}",
            label=r"GN2Xv00 deltaR",
            
        ),
        reference=True, #
    ),

    value.add_roc(
        Roc(
            sig_eff,
            Hbb_rej_ghost[0],
            n_test= np.sum(NJETS_ghost),
            rej_class="qcd",
            signal_class=f"{key}",
            label=r"GN2Xv00 ghostTracks",
            
        ),
        reference=False, #
    )

    value.set_ratio_class(1, "qcd")
    #value.make_linestyle_legend(
    #    linestyles=linestyles, labels = ["deltaR=1.0", "GhostTracks"], bbox_to_anchor=(0.8, 0.65)
    #)
    value.draw()
    value.savefig(f"roc_newData_discr_A2.png", transparent=False, dpi = 300)

print('Returned Cuts:')
print('GN2Xv00 deltaR: ')
print(Hbb_rej_deltar[1])
print('GN2Xv00 ghostTracks: ')
print(Hbb_rej_ghost[1])

